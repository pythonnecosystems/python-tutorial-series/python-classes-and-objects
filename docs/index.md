# Python 클래스와 객체: 생성과 사용 <sup>[1](#footnote_1)</sup>

> <font size="3">Python OOP에서 클래스와 객체를 생성하고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./classes-and-objects.md#intro)
1. [클래스와 객체란?](./classes-and-objects.md#sec_02)
1. [Python에서 클래스를 정의하는 방법](./classes-and-objects.md#sec_03)
1. [Python에서 객체를 만들고 액세스하는 방법](./classes-and-objects.md#sec_04)
1. [Python에서 클래스 속성과 메소드를 정의하고 사용하는 방법](./classes-and-objects.md#sec_05)
1. [Python에서 상속과 다형성을 사용하는 방법](./classes-and-objects.md#sec_06)
1. [Python에서 캡슐화와 추상화를 사용하는 방법](./classes-and-objects.md#sec_07)
1. [Python에서 매직 메서드와 연산자 오버로딩을 사용하는 방법](./classes-and-objects.md#sec_08)
1. [요약](./classes-and-objects.md#summary)


<a name="footnote_1">1</a>: [Python Tutorial 20 — Python Classes and Objects: Creation and Usage](https://python.plainenglish.io/python-tutorial-20-python-classes-and-objects-creation-and-usage-cc8a7daf2de8?sk=2cd8d22b44b2a4caf5b9b438a9e12bbf)를 편역하였다.
